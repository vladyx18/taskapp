const { initPlugin } = require('cypress-plugin-snapshots/plugin');
const wp = require('@cypress/webpack-preprocessor');

module.exports = (on: any, config: any) => {
  initPlugin(on, config);

  on('file:preprocessor', wp({ webpackOptions: require('../webpack.config.js') }));

  
  return config;
};
