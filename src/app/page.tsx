'use client';
import ClientComponent from '@/components/ClientComponent';
import { Login } from '@/pages/Login';
import { Taskapp } from '@/pages/Taskapp';
import { useEffect, useState } from 'react';
import { useDispatch } from
 'react-redux';
import { toggleLogin } from '@/redux/store';

export default function Home() {
  const [isAuthenticated, setIsAuthenticated] = useState(
    localStorage.getItem('authToken') ? true : false
  );

  const dispatch = useDispatch()
  
  useEffect(() => {
    if (isAuthenticated) {
      dispatch(toggleLogin(true));
    }
  }, [isAuthenticated, dispatch]);

  return (
    <main className="h-screen bg-center bg-cover">
      <ClientComponent>
        {isAuthenticated ? (
          <Taskapp />
        ) : (
          <Login />
        )}
      </ClientComponent>
    </main>
  );
}
