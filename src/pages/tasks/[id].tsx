'use client';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import 'tailwindcss/tailwind.css';

const TaskPage = () => {
  const router = useRouter();
  const { id } = router.query;
  const [newTask, setNewTask] = useState({
    title: '',
    description: '',
    completed: false,
  });

  const getData = async () => {
    const res = await fetch(`http://localhost:3000/api/tasks/${id}`);
    const data = await res.json();
    setNewTask({
      title: data.title,
      description: data.description,
      completed: data.completed,
    });
  };

  const handleDelete = async () => {
    try {
      if (window.confirm('Estas seguro que deseas eliminar la tarea?')) {
        await fetch(`http://localhost:3000/api/tasks/${id}`, {
          method: 'DELETE',
        });
        router.push('/');
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleConfirmTask = async () => {
    try {
      if (window.confirm('Estas seguro que deseas completar la tarea?')) {
        await fetch(`http://localhost:3000/api/tasks/${id}`, {
          method: 'PUT',
          body: JSON.stringify({ completed: true }),
          headers: {
            'Content-Type': 'application/json',
          },
        });
        router.push('/');
      }
    } catch (error) {
      console.log(error);
    }
  };

  const updatedTasks = async () => {
    try {
      const res = await fetch(`http://localhost:3000/api/tasks/${id}`, {
        method: 'PUT',
        body: JSON.stringify(newTask),
        headers: {
          'Content-Type': 'application/json',
        },
      });
      await res.json();
      router.push('/');
    } catch (error) {
      console.log(error);
    }
  };

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    updatedTasks();
  };

  useEffect(() => {
    if (id) {
      getData();
    }
  }, [id]);

  return (
    <div className="py-4 px-4">
      <div className="alert alert-info">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          className="stroke-current shrink-0 w-6 h-6"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2"
            d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
          ></path>
        </svg>
        <span>Tarea identificada: {id}.</span>
      </div>
      <div className="flex items-center justify-center h-screen">
        <div className="bg-white rounded-lg shadow-lg p-12 md:p-22 lg:p-32 xl:p-42">
          <div className="flex flex-col items-center">
            <h1 className="text-2xl mb-4 font-bold text-gray-900">
              EDITAR TAREA
            </h1>
            <form
              className="flex flex-col justify-center gap-4"
              onSubmit={handleSubmit}
            >
              <input
                type="text"
                placeholder="Titulo"
                className="input input-bordered input-primary"
                value={newTask.title}
                disabled={newTask.completed}
                onChange={(e) =>
                  setNewTask({ ...newTask, title: e.target.value })
                }
              />
              <textarea
                className="textarea textarea-primary"
                placeholder="Descripcion de la tarea"
                value={newTask.description}
                disabled={newTask.completed}
                onChange={(e) =>
                  setNewTask({ ...newTask, description: e.target.value })
                }
              ></textarea>
              <button
                type="submit"
                className="btn btn-primary"
                disabled={newTask.completed}
              >
                Guardar
              </button>
            </form>
            <div className="flex flex-row my-4 gap-2">
              <button className="btn btn-error" onClick={handleDelete}>
                Eliminar
              </button>
              <button
                className="btn btn-success"
                onClick={handleConfirmTask}
                disabled={newTask.completed}
              >
                Completar
              </button>
            </div>
            <div className="flex flex-row my-4 gap-2">
              <button
                className="btn btn-neutral"
                onClick={() => router.push('/')}
              >
                Regresar
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TaskPage;
