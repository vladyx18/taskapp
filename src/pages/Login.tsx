'use client';
import { useState } from 'react';
import { ModalAuth } from '@/components/ModalAuth';
import { AnimatePresence } from 'framer-motion';

export const Login = () => {
  const [isOpen, setIsOpen] = useState(false);

  const closeModal = () => {
    setIsOpen(false);
    return false;
  };

  return (
    <>
      <div
        className="hero min-h-screen bg-cover bg-center"
        style={{ backgroundImage: 'url(/img/background.jpg)' }}
      >
        <div className="hero-overlay bg-opacity-60"></div>
        <div className="hero-content text-center text-neutral-content">
          <div className="max-w-l">
            <h1 className="mb-5 text-5xl font-bold">
              Bienvenido a Task-App 📝
            </h1>
            <p className="mb-5">
              Tu app para organizar tus tareas y puedas administrar de mejor
              manera tu tiempo
            </p>
            <p className="mb-5">
              Para iniciar, pulsa el boton que hay debajo!👇
            </p>
            <button className="btn btn-warning" onClick={() => setIsOpen(true)}>
              Ingresa
            </button>
          </div>
        </div>
      </div>

      <AnimatePresence>
        {isOpen && <ModalAuth setIsClosed={closeModal} />}
      </AnimatePresence>
    </>
  );
};
