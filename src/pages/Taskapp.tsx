'use client';
import { motion, AnimatePresence } from 'framer-motion';
import {
  ModalCreateTask,
  Navbar,
  CardList,
} from '@/components/Index';
import { useSelector, useDispatch } from 'react-redux';
import {
  updateTasks,
  toggleModalOpen,
} from '@/redux/store';
import { useEffect } from 'react';

interface globalState {
  theme: 'light' | 'dark';
  isDark: boolean;
  isOpenModalTask: boolean;
}

interface Task {
  _id: string;
  title: string;
  description: string;
  completed: boolean;
  createdBy: string;
}

export const Taskapp = () => {
  const tasks = useSelector((state: { tasks: Task[] }) => state.tasks);

  const modalTaskOpen = useSelector(
    (state: { theme: globalState }) => state.theme.isOpenModalTask,
  );

  const dispatch = useDispatch();

  const closeModalTask = () => {
    dispatch(toggleModalOpen(false));
    return false;
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch('http://localhost:3000/api/tasks');
        const data = await response.json();
        console.log(data);
        dispatch(updateTasks(data));
      } catch (error) {
        console.error('Error al obtener las tareas', error);
        alert('Error al obtener las tareas. Por favor, inténtalo de nuevo.');
      }
    };

    fetchData();
  }, [dispatch]);

  return (
    <>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <Navbar />
        <div className="hero p-4 bg-base-200">
          <div className="hero-content text-center">
            <div className="max-w-xl">
              <h1 className="text-4xl font-bold">Bienvenido a Task-App 📝</h1>
              <p className="py-6">
                Aqui podras empezar a crear y gestionar cada una de tus tareas.
                ¡Empecemos!
              </p>
              <p className="text-4xl">
                👉{' '}
                <button
                  className="btn btn-primary"
                  onClick={() => dispatch(toggleModalOpen(true))}
                >
                  Crear Tarea
                </button>{' '}
                👈
              </p>
            </div>
          </div>
        </div>
        <div className="h-screen overflow-x-auto">
          <CardList cards={tasks} />
        </div>
      </motion.div>

      <AnimatePresence>
        {modalTaskOpen && <ModalCreateTask setIsClosed={closeModalTask} />}
      </AnimatePresence>

    </>
  );
};
