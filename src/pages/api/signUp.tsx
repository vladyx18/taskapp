import { NextApiRequest, NextApiResponse } from 'next';
import { connectDB } from '@/utils/mongoose';
import Auth from '@/models/Auth';
import bcrypt from 'bcryptjs';
import mongoose from 'mongoose';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  if (req.method === 'POST') {
    await handlePostRequest(req, res);
  } else {
    res.status(405).json({ error: 'Método no asociado al API' });
  }

  async function handlePostRequest(req: NextApiRequest, res: NextApiResponse) {
    try {
      await connectDB();
      const { email, password } = req.body;

      if (!password || password.length < 6) {
        return res
          .status(400)
          .json({ error: 'Tu contraseña tiene que ser mayor de 6 caracteres' });
      }

      const userFound = await Auth.findOne({ email });
      if (userFound)
        return res.status(400).json({
          error:
            'El correo que intenta registrar ya existe en la base de datos!',
        });

      const hashPwd = await bcrypt.hash(password, 12);
      const newAuth = new Auth({
        email,
        password: hashPwd,
      });
      const savedAuth = await newAuth.save();
      return res.status(201).json(savedAuth);
    } catch (error: any) {
      console.log(error);
      return res.status(500).json({ error: 'Error al crear un usuario!' });
    } finally {
      await mongoose.connection.close();
    }
  }
}
