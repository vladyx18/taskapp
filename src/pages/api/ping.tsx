import { NextApiRequest, NextApiResponse } from 'next';
import { connectDB } from '@/utils/mongoose';

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === 'GET') {
    connectDB();
    console.log('API GET request received');
    res.json({ message: 'Hello World!' });
  } else {
    res.status(405).json({ error: 'Method Not Allowed' });
  }
}
