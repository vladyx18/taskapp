import { NextApiRequest, NextApiResponse } from 'next';
import { connectDB } from '@/utils/mongoose';
import Auth from '@/models/Auth';
import bcrypt from 'bcryptjs';

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === 'POST') {
    await handlePostRequest(req, res);
  } else {
    res.status(405).json({ error: 'Método no asociado al API' });
  }

  async function handlePostRequest(req: NextApiRequest, res: NextApiResponse) {
    try {
      await connectDB();
      const { email, password } = req.body;

      const userFound = await Auth.findOne({ email }).select('email password');

      if (!userFound) {
        return res.status(401).json({ error: 'Credenciales incorrectas' });
      }

      const passwordMatch = await bcrypt.compare(password, userFound.password);

      if (!passwordMatch) {
        return res.status(401).json({ error: 'Credenciales incorrectas' });
      }

      return res.status(200).json({ message: 'Inicio de sesión exitoso' });
    } catch (error: any) {
      console.error(error);
      return res.status(500).json({ error: 'Error al iniciar sesión' });
    }
  }
}
