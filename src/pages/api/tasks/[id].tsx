import { NextApiRequest, NextApiResponse } from 'next';
import { connectDB } from '@/utils/mongoose';
import Task from '@/models/Task';
import mongoose from 'mongoose';

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  const { id } = req.query;

  if (req.method === 'GET') {
    handleGetRequest(req, res, id as string);
  } else if (req.method === 'DELETE') {
    handleDeleteRequest(req, res, id as string);
  } else if (req.method === 'PUT') {
    handlePutRequest(req, res, id as string);
  } else {
    res.status(405).json({ error: 'Metodo no asociado al API' });
  }

  async function handleGetRequest(
    req: NextApiRequest,
    res: NextApiResponse,
    id: string,
  ) {
    try {
      await connectDB();
      const taskFound = await Task.findById(id);
      if (!taskFound) {
        res.status(404).json({ error: 'Tarea no encontrada' });
      } else {
        res.json(taskFound);
      }
    } catch (error: any) {
      console.log(error);
      res.status(500).json({ error: 'Error al obtener la tarea!' });
    } finally {
      await mongoose.connection.close();
    }
  }

  async function handlePutRequest(
    req: NextApiRequest,
    res: NextApiResponse,
    id: string,
  ) {
    try {
      await connectDB();
      const data = req.body;
      const taskUpdated = await Task.findByIdAndUpdate(id, data, { new: true });
      res.json(taskUpdated);
    } catch (error: any) {
      console.log(error);
      res.status(500).json({ error: 'Error al actualizar la tarea!' });
    } finally {
      await mongoose.connection.close();
    }
  }

  async function handleDeleteRequest(
    req: NextApiRequest,
    res: NextApiResponse,
    id: string,
  ) {
    try {
      await connectDB();
      const taskDeleted = await Task.findByIdAndDelete(id);
      if (!taskDeleted) {
        res.status(404).json({ error: 'Tarea no encontrada' });
      } else {
        res.json(taskDeleted);
      }
    } catch (error: any) {
      console.log(error);
      res.status(500).json({ error: 'Error al Eliminar la tarea!' });
    } finally {
      await mongoose.connection.close();
    }
  }
}
