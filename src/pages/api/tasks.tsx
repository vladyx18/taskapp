import { NextApiRequest, NextApiResponse } from 'next';
import { connectDB } from '@/utils/mongoose';
import Task from '@/models/Task';
import mongoose from 'mongoose';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  if (req.method === 'GET') {
    await handleGetRequest(req, res);
  } else if (req.method === 'POST') {
    await handlePostRequest(req, res);
  } else {
    res.status(405).json({ error: 'Método no asociado al API' });
  }

  async function handleGetRequest(req: NextApiRequest, res: NextApiResponse) {
    try {
      await connectDB();
      const tasks = await Task.find();
      return res.json(tasks);
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error: 'Error al obtener las tareas!' });
    } finally {
      await mongoose.connection.close();
    }
  }

  async function handlePostRequest(req: NextApiRequest, res: NextApiResponse) {
    try {
      await connectDB();
      const data = req.body;
      const newTask = new Task(data);
      const savedTask = await newTask.save();

      return res.status(201).json(savedTask);
    } catch (error: any) {
      if (error.code === 11000) {
        return res
          .status(400)
          .json({ error: 'Ya existe una tarea con este título' });
      }

      console.log(error);
      return res.status(500).json({ error: 'Error al crear una tarea!' });
    } finally {
      await mongoose.connection.close();
    }
  }
}
