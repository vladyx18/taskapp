import { motion } from 'framer-motion';
import { useState, FormEvent } from 'react';
import { toggleLogin } from '@/redux/store';
import { useDispatch } from 'react-redux';

interface ModalAuthProps {
  setIsClosed: () => boolean;
}

export const ModalAuth = ({ setIsClosed }: ModalAuthProps) => {
  const [isRegister, setIsRegister] = useState(false);
  const [isError, setIsError] = useState('');

  const dispatch = useDispatch()
  
  const handleChange = () => {
    setIsRegister(!isRegister);
    return isRegister;
  };

  const handleSubmitLog = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const formData = new FormData(e.currentTarget);

    const email = formData.get('email');
    const password = formData.get('password');

    try {
      const res = await fetch('http://localhost:3000/api/signOn', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email, password }),
      });

      if (res.ok) {
        localStorage.setItem('authToken', '12345');
        dispatch(toggleLogin(true));
        window.location.reload()
      } else {
        const errorBody = await res.json();
        alert('Login fallido, revise sus credenciales!');
        setIsError(errorBody.error);
      }
    } catch (error: any) {
      console.log(error);
    }
  };

  const handleSubmitReg = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const formData = new FormData(e.currentTarget);

    const email = formData.get('email');
    const password = formData.get('password');

    try {
      const res = await fetch('http://localhost:3000/api/signUp', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email, password }),
      });

      if (res.ok) {
        setIsClosed();
        alert('Usuario creado con exito!');
      } else {
        const errorBody = await res.json();
        alert('No se pudo crear el usuario!');
        setIsError(errorBody.error);
      }
    } catch (error: any) {
      console.log(error);
    }
  };

  return (
    <motion.div
      className="fixed inset-0 flex items-center justify-center z-50 bg-gray-800 bg-opacity-50"
      initial={{ y: '100vh' }}
      animate={{ y: 0 }}
      exit={{ y: '100vh' }}
      transition={{ type: 'spring', stiffness: 50 }}
    >
      <div className="modal modal-open">
        <div className="modal-box lg:w-1/2 md:w-3/4 w-11/12 mx-auto">
          <div className="card-title flex flex-row-reverse">
            <button
              className="btn btn-circle btn-outline"
              onClick={setIsClosed}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M6 18L18 6M6 6l12 12"
                />
              </svg>
            </button>
          </div>
          {isError && (
            <div className="alert alert-error w-72 mx-auto my-4">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="stroke-current shrink-0 h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"
                />
              </svg>
              <span>{isError}</span>
            </div>
          )}
          <div className="flex flex-row justify-center">
            <h1 className="font-bold xl:text-xl lg:text-lg md:text-lg sm:text-sm xs:text-xs">
              {`${isRegister ? 'Registrarse' : 'Iniciar Sesión'}`}
            </h1>
          </div>
          <form
            className="card-body -mt-4"
            onSubmit={isRegister ? handleSubmitReg : handleSubmitLog}
          >
            <div className="form-control">
              <label className="label">
                <span className="label-text">Correo Electronico</span>
              </label>
              <input
                type="email"
                placeholder="Correo Electronico"
                name="email"
                className="input input-bordered"
                required
              />
            </div>
            <div className="form-control">
              <label className="label">
                <span className="label-text">Contraseña</span>
              </label>
              <input
                type="password"
                placeholder="Contraseña"
                name="password"
                className="input input-bordered"
                required
              />
              <label className="label">
                <a
                  href="#"
                  className="label-text-alt link link-hover"
                  onClick={handleChange}
                >
                  {`${
                    isRegister
                      ? 'Ya tienes cuenta?, Inicia Sesión'
                      : 'No tienes cuenta?, Registrate!'
                  }`}
                </a>
              </label>
            </div>
            <div className="form-control mt-6">
              {isRegister ? (
                <button className="btn btn-secondary" type="submit">
                  Registrarse
                </button>
              ) : (
                <button type="submit" className="btn btn-primary">
                  Ingresar
                </button>
              )}
            </div>
          </form>
        </div>
      </div>
    </motion.div>
  );
};
