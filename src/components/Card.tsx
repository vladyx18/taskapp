import Link from 'next/link';

interface CardProps {
  id: string;
  title: string;
  description: string;
  completed: boolean;
}

export const Card: React.FC<CardProps> = ({
  id,
  title,
  description,
  completed,
}) => {

  return (
    <Link href={`/tasks/${id}`}>
      <div className="card w-60 md:w-96 lg:w-96 xl:w-96 bg-base-100 shadow-xl">
        <div className="card-body" onClick={() => {}}>
          <h2 className="card-title">{`${
            completed ? '✅' : '⏳'
          } ${title}`}</h2>
          {/* ⏳ ✅ */}
          <p className="card-description break-words">{description}</p>
        </div>
      </div>
    </Link>
  );
};
