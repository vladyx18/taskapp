'use client';
import { ReactNode, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { toggleTheme } from '@/redux/store';

const ClientComponent = ({ children }: { children: ReactNode }) => {
  const theme = useSelector(
    (state: { theme: { theme: string } }) => state.theme.theme,
  );

  const dispatch = useDispatch()

  useEffect(() => {
    const storedTheme = localStorage.getItem('theme')
    if(storedTheme){
      dispatch(toggleTheme())
    }
  }, [dispatch])


  return (
    <div className="bg-primary" data-theme={theme}>
      {children}
    </div>
  );
};

export default ClientComponent;
