import { addTask } from '@/redux/store';
import { motion } from 'framer-motion';
import { useState } from 'react';
import { useDispatch } from 'react-redux';

interface ModalCreateTaskProps {
  setIsClosed: () => boolean;
}

export const ModalCreateTask = ({ setIsClosed }: ModalCreateTaskProps) => {
  const [taskData, setTaskData] = useState({
    title: '',
    description: '',
    completed: false,
    createdBy: 'Admin',
  });

  const dispatch = useDispatch();

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    try {
      const response = await fetch('http://localhost:3000/api/tasks', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(taskData),
      });

      if (response.ok) {
        setIsClosed();
        const updatedTasksResponse = await fetch(
          'http://localhost:3000/api/tasks',
        );
        const updatedTasks = await updatedTasksResponse.json();
        dispatch(addTask(updatedTasks));
      }
    } catch (error) {
      console.error('Error al enviar los datos:', error);
    }
  };

  return (
    <motion.div
      className="fixed inset-0 flex items-center justify-center z-50 bg-gray-800 bg-opacity-50"
      initial={{ y: '100vh' }}
      animate={{ y: 0 }}
      exit={{ y: '100vh' }}
      transition={{ type: 'spring', stiffness: 50 }}
    >
      <div className="modal modal-open">
        <div className="modal-box lg:w-1/2 md:w-3/4 w-11/12 mx-auto">
          <div className="card-title flex flex-row-reverse">
            <button
              className="btn btn-circle btn-outline"
              onClick={setIsClosed}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M6 18L18 6M6 6l12 12"
                />
              </svg>
            </button>
          </div>
          <div className="flex flex-row justify-center">
            <h1 className="font-bold xl:text-xl lg:text-lg md:text-lg sm:text-sm xs:text-xs">
              Crea una nueva Tarea
            </h1>
          </div>
          <form
            className="card-body flex flex-column justify-center -mt-4"
            onSubmit={handleSubmit}
          >
            <input
              type="text"
              placeholder="Nombre de la tarea"
              value={taskData.title}
              onChange={(e) =>
                setTaskData({ ...taskData, title: e.target.value })
              }
              className="input input-bordered input-primary w-full max-w-xl"
            />
            <textarea
              className="textarea textarea-primary"
              placeholder="Descripcion de la tarea"
              value={taskData.description}
              onChange={(e) =>
                setTaskData({ ...taskData, description: e.target.value })
              }
            ></textarea>
            <div className="form-control mt-6">
              <button type="submit" className="btn btn-primary">
                Ingresar
              </button>
            </div>
          </form>
        </div>
      </div>
    </motion.div>
  );
};
