import { Card } from './Card';

interface CardData {
  _id: string;
  title: string;
  description: string;
  completed: boolean;
}

interface CardListProps {
  cards: CardData[];
}

export const CardList: React.FC<CardListProps> = ({ cards }) => {
  if (!Array.isArray(cards)) {
    return null;
  }

  return (
    <div className="card-list flex flex-row flex-wrap justify-center gap-4 py-12">
      {cards.map((card, index) => (
        <Card
          key={index}
          id={card._id}
          title={card.title}
          description={card.description}
          completed={card.completed}
        />
      ))}
    </div>
  );
};
