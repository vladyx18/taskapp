import Image from 'next/image';
import { useDispatch, useSelector } from 'react-redux';
import { toggleTheme, toggleLogin } from '@/redux/store';

interface globalState {
  theme: 'light' | 'dark';
  isDark: boolean;
  isOpenModalTask: boolean;
}

export const Navbar = () => {
  const isDark = useSelector(
    (state: { theme: globalState }) => state.theme.isDark,
  );
  const dispatch = useDispatch();


  const handleToggleTheme = () => {
    dispatch(toggleTheme());
    localStorage.setItem('theme', isDark ? 'light' : 'dark')
  };

  const handleDestroySession = () => {
    localStorage.removeItem('authToken');
    localStorage.clear();
    dispatch(toggleLogin(false))
    window.location.reload()
  }

  return (
    <div className="navbar bg-base-100">
      <div className="flex-1">
        <a className="btn btn-ghost normal-case text-xl">Task-App 📝</a>
      </div>
      <div className="flex-none gap-2">
        <div className="dropdown dropdown-end">
          <label
            className="btn btn-neutral btn-circle"
            onClick={handleToggleTheme}
          >
            {isDark ? (
              <Image src="/sun.svg" alt="Modo Claro" width={32} height={32} />
            ) : (
              <Image src="/moon.svg" alt="Modo Claro" width={32} height={32} />
            )}
          </label>
        </div>
        <div className="dropdown dropdown-end">
          <label tabIndex={0} className="btn btn-ghost btn-circle avatar">
            <div className="w-10 rounded-full">
              <img src="/img/avatar.png" />
            </div>
          </label>
          <ul
            tabIndex={0}
            className="menu menu-sm dropdown-content mt-3 z-[1] p-2 shadow bg-base-100 rounded-box w-52"
          >
            <li onClick={handleDestroySession}>
              <a>Cerrar Sesión</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};
