import { createSlice, configureStore, PayloadAction } from '@reduxjs/toolkit';

interface globalState {
  theme: string;
  isDark: boolean;
  isOpenModalTask: boolean;
  isLoggedIn: boolean, 
}

interface Task {
  id: string;
  title: string;
  description: string;
  completed: boolean;
  createdBy: string;
}

const themeFromLocalStorage: string | null = localStorage.getItem('theme');

const initialState: globalState = {
  theme: themeFromLocalStorage || 'light',
  isDark: themeFromLocalStorage === 'dark',
  isOpenModalTask: false,
  isLoggedIn: false,
};

const globalSlice = createSlice({
  name: 'theme',
  initialState,
  reducers: {
    toggleTheme: (state) => {
      state.theme = state.theme === 'light' ? 'dark' : 'light';
      state.isDark = !state.isDark;
    },
    toggleModalOpen: (state, action: PayloadAction<boolean>) => {
      state.isOpenModalTask = action.payload;
    },
    toggleLogin: (state, action: PayloadAction<boolean>) => {
      state.isLoggedIn = action.payload;
    },
  },
});

const tasksSlice = createSlice({
  name: 'tasks',
  initialState: [] as Task[],
  reducers: {
    updateTasks: (state, action: PayloadAction<Task[]>) => {
      const incomingTasks = action.payload;

      const existingTaskIds = new Set(state.map((task) => task.id));

      const uniqueNewTasks = incomingTasks.filter(
        (task) => !existingTaskIds.has(task.id),
      );

      if (uniqueNewTasks.length > 0) {
        const updatedState = [...state, ...uniqueNewTasks];
        return updatedState;
      }

      return state;
    },
    addTask: (state, action: PayloadAction<Task[]>) => {
      return action.payload;
    },
    removeTask: (state, action: PayloadAction<string>) => {
      const taskIdToRemove = action.payload;
      const updatedState = state.filter((task) => task.id !== taskIdToRemove);
      return updatedState;
    },
  },
});

export const { toggleTheme, toggleModalOpen, toggleLogin } =
  globalSlice.actions;
export const { updateTasks, addTask, removeTask } = tasksSlice.actions;

const store = configureStore({
  reducer: {
    theme: globalSlice.reducer,
    tasks: tasksSlice.reducer,
  },
});

export default store;
