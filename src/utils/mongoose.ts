import { connect, connection } from 'mongoose';

const conn = {
  isConnected: false,
};

export async function connectDB() {
  if (conn.isConnected) return;

  try {
    const db = await connect('mongodb://localhost:27017/taskAppDB');
    console.log('Mongoose está conectado!');
    console.log(`Base de datos conectada: ${db.connection.db.databaseName}`);
    conn.isConnected = Boolean(db.connections[0].readyState);
  } catch (error) {
    console.error('Error al conectar con Mongoose', error);
  }
}

connection.on('error', (err) => {
  console.error('Error de conexión con Mongoose:', err);
});

connection.on('disconnected', () => {
  console.log('Mongoose se desconectó');
  conn.isConnected = false;
});

process.on('SIGINT', async () => {
  await connection.close();
  console.log(
    'Conexión de Mongoose cerrada debido a la terminación de la aplicación',
  );
  process.exit(0);
});
