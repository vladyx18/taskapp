import { Schema, model, models } from 'mongoose';

const authSchema = new Schema(
  {
    email: {
      type: String,
      required: [true, 'El email es requerido'],
      unique: true,
      match: [
        /^\w+([.-_+]?\w+)*@\w+([.-]?\w+)*(\.\w{2,10})+$/,
        'Su correo es invalido!',
      ],
    },
    password: {
      type: String,
      required: [true, 'La contraseña es requerida'],
    },
  },
  {
    timestamps: true,
  },
);

export default models.Auth || model('Auth', authSchema);
